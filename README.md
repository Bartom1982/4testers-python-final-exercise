# 4testers 
## Python Programming - Final Assignment

### Instructions
In the `src` module, you will find a file called `shop.py`, which contains two classes:

- `Product`, which describes a product in the shop with a name, price, and ordered quantity.
- `Order`, which describes an order in the shop, with the ability to add a product, calculate the total price, total quantity of products, and complete a purchase.
Additionally, in the tests module, there are tests for these classes. Unfortunately, a mischievous bug has removed the logic from these classes. However, it forgot to delete the tests.

Your task is to fill in the methods in both classes based on what you find in the tests, so that without changing the test code, they pass successfully (i.e., turn "green").

Good luck!

***************************************************************

# 4testers 
## Programowanie w Python - zadanie zaliczeniowe

### Instrukcja
W module `src` znajdziesz plik `shop.py` Zawierający dwie klasy:
- `Product`, który opisuje produkt w sklepie, z nazwą, ceną i zamawianą ilością
- `Order`, który opsuje zamówienie w sklepie, z możliwością dodania produktu, policzenia całkowitej ceny, całkowitej ilości produktów oraz zakupu
Oprócz tego w module `tests` znajdują się testy do tych klas. 
Niestety, zły chochlik usunął logikę tych klas. Zapomniał natomiast wyczyścić testy.

Twoim zadaniem jest uzupełneinie metod w obu klasach na podstawie tego, co znajdziesz w testach, tak aby **nie zmieniając kodu testów** przeszły one "na zielono".

Powodzenia!